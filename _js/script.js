$(document).ready(function(){

	const ITEMS_ON_PAGE = 5;
	const EMPTY_LINE = /^\s*$/;
	const ENTER_KEY = 'Enter';

	let todos = [];
	let todosIdCounter = 0;
	let todoText = '';
	let currentTab = 'all';
	let currentPageIndex = 0;

	const $todoList = $('#todo-list');
	const $mainTextField = $('#main-textfield');
	const $enterButton = $('#enter-button');
	const $wrapper = $('#wrapper');
	const $checkAll = $('#check-all').children('input');
	const $checkedCounter = $('#counters__checked__number');
	const $uncheckedCounter = $('#counters__unchecked__number');
	const $tabs = $('#tabs');
	const $bookmarks = $('#bookmarks');

	const getCheckedTotal = function() {
		return todos.reduce(function(value, item){
			return value + Number(item.checked);
		}, 0);
	};

	const getUncheckedTotal = function() {
		return todos.reduce(function(value, item){
			return value + Number(!item.checked);
		}, 0);
	};

	const getLastPageIndex = function() {
		let pagesTotal;
		switch (currentTab) {
			case 'all': pagesTotal = Math.ceil(todos.length / ITEMS_ON_PAGE);
			break;
			case 'active': pagesTotal = Math.ceil(getUncheckedTotal() / ITEMS_ON_PAGE);
			break;
			case 'completed': pagesTotal = Math.ceil(getCheckedTotal() / ITEMS_ON_PAGE);
		}
		return pagesTotal - 1;
	};

	const redrawDOM = function(array) {
		$bookmarks.empty();
		let bookmarksAll = '';
		for (let i = 0; i <= getLastPageIndex(); i++) {
			bookmarksAll += `<li class=\"btn btn-light bookmark${
				i === currentPageIndex ? ' active-bookmark' : ''}\">Page ${i + 1}</li>`;
		}
		$bookmarks.html(bookmarksAll);
		$todoList.empty();
		let task = '';
		array.forEach(function(item){
			task += `<li id=\"${item.id}\"><input type=\"checkbox\" maxlength=\"30\" ${
				item.checked === true ? 'checked' : ''}><span>${
					_.escape(item.text)}</span><div class=\"todo-list__delete-button\">x</div></li>`;
		});
		$todoList.html(task);
		$checkedCounter.text(getCheckedTotal());
		$uncheckedCounter.text(getUncheckedTotal());
	};

	const render = function() {
		let lastPageIndex = getLastPageIndex();
		if (currentPageIndex > lastPageIndex)
			currentPageIndex = lastPageIndex;
		else if (currentPageIndex < 0) currentPageIndex = 0;
		let beginIndex = currentPageIndex * ITEMS_ON_PAGE;
		let endIndex = beginIndex + ITEMS_ON_PAGE;
		switch (currentTab) {
			case 'all': redrawDOM(todos.concat().slice(beginIndex, endIndex));
			break;
			case 'active': redrawDOM(todos.concat().filter(function(item){
				return item.checked === false;
			}).slice(beginIndex, endIndex));
			break;
			case 'completed': redrawDOM(todos.concat().filter(function(item){
				return item.checked === true;
			}).slice(beginIndex, endIndex));
		}
	};

	const updateCheckAllCheckbox = function() {
		let allTodosChecked = todos.every(function(item){
			return item.checked;
		});
		if ((todos.length === 0) || (!allTodosChecked))
			$checkAll.prop('checked', false);
		else $checkAll.prop('checked', true);
	};

	const addElement = function(event) {
		if ((event.type === 'click') || (event.key === ENTER_KEY)) {
			todoText = $mainTextField.val().trim();
			if (!EMPTY_LINE.test(todoText)) {
				let todoObject = {id: todosIdCounter, text: todoText, checked: false};
				todos.push(todoObject);
				todosIdCounter++;
				$mainTextField.val('');
				updateCheckAllCheckbox();
				currentPageIndex = getLastPageIndex();
				render();
			}
		}
	};

	const deleteElement = function() {
		let todoIdToDelete = Number($(this).parent().prop('id'));
		todos = todos.filter(function(item){
			return item.id !== todoIdToDelete;
		});
		updateCheckAllCheckbox();
		render();
	};

	const deleteAllChecked = function() {
		todos = todos.filter(function(item){
			return item.checked === false;
		});
		updateCheckAllCheckbox();
		render();
	};

	const changeCheckbox = function() {
		let thisCheckboxId = Number($(this).parent().prop('id'));
		let thisCheckboxStatus = $(this).prop('checked');
		todos.forEach(function(item){
			if (item.id === thisCheckboxId)
				item.checked = thisCheckboxStatus;
		});
		updateCheckAllCheckbox();
		render();
	};

	const changeCheckboxAll = function() {
		let checkAllStatus = $(this).prop('checked');
		todos.forEach(function(item){
			item.checked = checkAllStatus;
		});
		updateCheckAllCheckbox();
		render();
	};

	const showDeleteButton = function() {
		$(this).children('.todo-list__delete-button').css('display', 'inline-block');
	};

	const hideDeleteButton = function() {
		$(this).children('.todo-list__delete-button').css('display', 'none');
	};

	const doubleClickEdit = function() {
		let $thisTodoSpan = $(this).children('span');
		let $thisTodoInput = $(this).children('input');
		todoText = $thisTodoSpan.text();
		$thisTodoSpan.text('');
		$thisTodoInput.prop('type', 'text');
		$thisTodoInput.focus().val(todoText);
	};

	const enterAfterEdit = function(event) {
		if (event.key === ENTER_KEY) {
			let $this = $(this);
			let thisText = $this.val().trim();
			let thisId = Number($this.parent().prop('id'));
			if (!EMPTY_LINE.test(thisText)) {
				todos.forEach(function(item){
					if (item.id === thisId)
						item.text = thisText;
				});
				render();
			}
		}
	};

	const blurAfterEdit = function() {
		let $this = $(this);
		$this.prop('type', 'checkbox');
		$this.siblings('span').text(todoText);
	};

	const changeTab = function() {
		let $this = $(this);
		$this.siblings().removeClass('active-tab');
		$this.addClass('active-tab');
		currentTab = $this.text();
		render();
	};

	const choosePageByBookmark = function() {
		currentPageIndex = $(this).index();
		render();
	};

	$mainTextField.keypress(addElement);
	$enterButton.click(addElement);
	$wrapper.on('change', '#check-all input', changeCheckboxAll);
	$todoList.on('change', 'input:checkbox', changeCheckbox);
	$todoList.on('mouseenter', 'li', showDeleteButton);
	$todoList.on('mouseleave', 'li', hideDeleteButton);
	$todoList.on('click', '.todo-list__delete-button', deleteElement);
	$wrapper.on('click', '#delete-checked', deleteAllChecked);
	$todoList.on('dblclick', 'li', doubleClickEdit);
	$todoList.on('keypress', 'input:text', enterAfterEdit);
	$todoList.on('blur', 'input:text', blurAfterEdit);
	$tabs.on('click', 'div', changeTab);
	$bookmarks.on('click', 'li', choosePageByBookmark);

});